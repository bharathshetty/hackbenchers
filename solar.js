
///////////////////////////////////////////////////
//////////////// Set the Scales ///////////////////
///////////////////////////////////////////////////
  
//Set the dimensions of the chart

  

d3.csv("http://localhost:8000/solar.csv", function(planets) {
 var margin = {top: 100, right: 30, bottom: 30, left: 30},
     width = 1000 - margin.left - margin.right,
     height = 1000 - margin.top - margin.bottom;

  planets.forEach(function(d) {
    d.RadiusJpt = +d.RadiusJpt;
  });
  
	//I'm using linear, because I already know the exact diameter and don't want to do an area comparison
	var planetScale = d3.scale.linear()
		.domain([0, d3.max(planets, function(d) {  return d.RadiusJpt; })])
		.range([0, 200]);
	  
	//Create an offset for each planet,based on the diameters of the previous planets
	var padding = 10;	
	planets.forEach( function(d,i) {
		
		if(i === 0) { 
			d.offset = 0; 
		} else {
			d.offset = planets[i-1].offset + planetScale(planets[i-1].RadiusJpt) + padding*2;
		}
	});

	      
	//Add the svg canvas
	var svg = d3.select("#chart")
	    .append("svg")
	        .attr("width", width + margin.left + margin.right)
	        .attr("height", height + margin.top + margin.bottom)
	    .append("g")
	        .attr("transform", "translate(" + (margin.left) + "," + margin.top + ")");
	  
	///////////////////////////////////////////////////
	///////// Create sphere looking planets ///////////
	///////////////////////////////////////////////////
	var color = d3.scale.category10();	
	//Radial gradient with the center at one end of the circle, as if illuminated from the side
	var gradientRadial = svg.append("defs").selectAll("radialGradient")
		.data(planets)
		.enter().append("radialGradient")
		.attr("id", function(d){ return "gradient-" + d.PlanetIdentifier; })
		.attr("cx", "30%")
		.attr("cy", "30%")
		.attr("r", "65%");


	//Append the color stops
	gradientRadial.append("stop")
		.attr("offset", "0%")
		.attr("stop-color", function(d) { return d3.rgb(d.colors).brighter(1); });
	gradientRadial.append("stop")
		.attr("offset", "50%")
		.attr("stop-color", function(d) { return d.colors; });
	gradientRadial.append("stop")
		.attr("offset",  "100%")
		.attr("stop-color", function(d) { return d3.rgb(d.colors).darker(1.5); });
		
	//Add the planets
	var plantes_grad = svg.selectAll(".planetsGradient")
		.data(planets)
		.enter()
		.append("g");

	plantes_grad.append('circle')		
		.attr("class", "planetsGradient")
		.attr("cy", function(d, i) { return d.offset + planetScale(d.RadiusJpt)/2 + padding; })
		.attr("cx", 100)
		.attr("r", function(d) { return planetScale(d.RadiusJpt)/2; })
		.style("fill", function(d) { return "url(#gradient-" + d.PlanetIdentifier + ")"; })

	});

