 // The zomable plot and the tables goes here in this file 

 var w = window,
     d = document,
     e = d.documentElement,
     g = d.getElementsByTagName('body')[0],
     wi = w.innerWidth || e.clientWidth || g.clientWidth,
     he = w.innerHeight|| e.clientHeight|| g.clientHeight;


var margin = { top: 50, right: 0, bottom: 50, left: 50 },
    outerWidth = wi>400?wi/2.1:wi/1.3,
    outerHeight = 500,
    width = outerWidth - margin.left - margin.right,
    height = outerHeight - margin.top - margin.bottom;

var x = d3.scale.linear()
    .range([0, width]).nice();

var y = d3.scale.linear()
    .range([height, 0]).nice();

var xCat = "DEC",
    yCat = "RA",
    rCat = "RadiusJpt",
    colorCat = "ListsPlanetIsOn";

d3.csv("/data/to_display.csv", function(data) {
  data.forEach(function(d) {
    d.DEC = +d.DEC;
    d.RA = +d.RA;
    d.RadiusJpt = +d.RadiusJpt;
    d.SurfaceTempK = +d.SurfaceTempK;
    d.HostStarMassSlrMass = +d.HostStarMassSlrMass;
  });

  var xMax = d3.max(data, function(d) { return d[xCat]; }) * 1.05,
      xMin = d3.min(data, function(d) { return d[xCat]; }),
      xMin = xMin > 0 ? 0 : xMin,
      yMax = d3.max(data, function(d) { return d[yCat]; }) * 1.05,  
      yMin = d3.min(data, function(d) { return d[yCat]; }),
      yMin = yMin > 0 ? 0 : yMin;

  x.domain([xMin, xMax]);
  y.domain([yMin, yMax]);

  var xAxis = d3.svg.axis()
      .scale(x)
      .orient("bottom")
      .tickSize(-height);

  var yAxis = d3.svg.axis()
      .scale(y)
      .orient("left")
      .tickSize(-width);

  var color = d3.scale.category10();

  var tip = d3.tip()
      .attr("class", "d3-tip")
      .offset([-10, 0])
      .html(function(d) {
        return "Name: " + d['PlanetIdentifier'] + "<br>" + 
               "Surface Temperature in (K): " + d["SurfaceTempK"] + "<br>" + 
               "Distance From Sun in (Parsec): "  + d["DistFromSunParsec"] + "<br>" + 
               "Planetary Mass (Jpt): "   + d["PlanetaryMassJpt"] + "<br>" +
               "Radius (Jpt): " + d["RadiusJpt"] + "<br>" ;
      });

  var zoomBeh = d3.behavior.zoom()
      .x(x)
      .y(y)
      .scaleExtent([0, 5000])
      .on("zoom", zoom);

  var svg = d3.select("#scatter")
    .append("svg")
      .attr("width", outerWidth)
      .attr("height", outerHeight)
    .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
      .call(zoomBeh);

  svg.call(tip);

  svg.append("rect")
      .attr("width", width)
      .attr("height", height);

  svg.append("g")
      .classed("x axis", true)
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis)
      .append("text")
      .classed("label", true)
      .attr("x", width)
      .attr("y", margin.bottom - 10)
      .style("text-anchor", "end")
      .text('Latitude');

  svg.append("text")
        .attr("x", (width / 2))             
        .attr("y", 0 - (margin.top / 2))
        .attr("text-anchor", "middle")  
        .style("font-size", "16px") 
        .style("text-decoration", "none")  
        .text("Zoomable plot of exo planets");

  svg.append("g")
      .classed("y axis", true)
      .call(yAxis)
    .append("text")
      .classed("label", true)
      .attr("transform", "rotate(-90)")
      .attr("y", -margin.left)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text('Longitude');

  var objects = svg.append("svg")
      .classed("objects", true)
      .attr("width", width)
      .attr("height", height);

  objects.append("svg:line")
      .classed("axisLine hAxisLine", true)
      .attr("x1", 0)
      .attr("y1", 0)
      .attr("x2", width)
      .attr("y2", 0)
      .attr("transform", "translate(0," + height + ")");

  objects.append("svg:line")
      .classed("axisLine vAxisLine", true)
      .attr("x1", 0)
      .attr("y1", 0)
      .attr("x2", 0)
      .attr("y2", height);

  objects.selectAll(".dot")
      .data(data)
      .enter().append("circle")
      .classed("dot", true)
      .attr("r", function (d) { return 6 * Math.sqrt(d[rCat] / Math.PI); })
      .attr("transform", transform)
      .style("fill", function(d) { return color(d[colorCat]); })
      .on("mouseover", tip.show)
      .on("mouseout", tip.hide)
      .on("click", click);
  function click(d) 
  {
    document.getElementById('planetarytitle').innerHTML=`
        <h5>${d.PlanetIdentifier}</h5> 
    `;
    document.getElementById("planetaryinfo").innerHTML = `
    <p>
      <div class="col s12">
         This planet is of type <b> "${d.TypeFlag}" </b> and has a Planetary Mass of <b>${d.PlanetaryMassJpt} Jpt</b> and has a radius of <b>${d.RadiusJpt} Jpt</b>. Its surface temperature is about <b>${d.SurfaceTempK}</b> K. It was discovered using <b>${d.DiscoveryMethod}</b> technique. 
           <br/> <br/> It was discovered in the year <b>${d.DiscoveryYear.slice(0,-2)}</b>. Its Right Ascension  is <b>${d.RightAscension}</b> and its Declination  is <b>${d.Declination}</b>. Its about <b>${d.DistFromSunParsec}</b> Parsecs away from our sun. Its Host Star Mass is about <b>${d.HostStarMassSlrMass}</b> Slr Mass and Radius is about <b>${d.HostStarRadiusSlrRad}</b> Slr Rad. Host star metallicity is ${d.HostStarMetallicity}, and the temperature is about <b>${d.HostStarTempK}</b> K.This planet is under <b>${d.ListsPlanetIsOn}</b>.       
      </div>
    </p>      
    `;
    console.log(typeof(d.DiscoveryYear)); //considering dot has a title attribute
  }

  var svg2 = d3.select("#scatter")
    .append("svg")
      .attr("width",window.innerWidth-100)
      .attr("height", wi>800? 150:250)
      .append("g")
      .attr("transform", "translate(" + -850 + "," + 20 + ")")

 
  var n = wi <= 800 ? 1 : 3;
  var itemWidth = 200;
  var itemHeight = 18;

  var legend = svg2.selectAll(".legend")
      .data(color.domain())
      .enter().append("g")
      .classed("legend", true)
      .attr("transform", function(d,i) { 
        return "translate(" + i%n * itemWidth + "," + Math.floor(i/n) * itemHeight + ")";
      });

  legend.append("circle")
      .attr("r", 3.5)
      .attr("cx", 920-10)
      .attr("fill", color);

  legend.append("text")
      .attr("x", 920)
      .attr("dy", ".35em")
      .style("font-size", "14px")
      .text(function(d) { return d; });

  d3.select("input").on("click", change);

  function change() {
    xCat = "Carbs";
    xMax = d3.max(data, function(d) { return d[xCat]; });
    xMin = d3.min(data, function(d) { return d[xCat]; });

    zoomBeh.x(x.domain([xMin, xMax])).y(y.domain([yMin, yMax]));

    var svg = d3.select("#scatter").transition();

    svg.select(".x.axis").duration(750).call(xAxis).select(".label").text(xCat);

    objects.selectAll(".dot").transition().duration(1000).attr("transform", transform);
  }

  function zoom() {
    svg.select(".x.axis").call(xAxis);
    svg.select(".y.axis").call(yAxis);

    svg.selectAll(".dot")
        .attr("transform", transform);
  }

  function transform(d) {
    return "translate(" + x(d[xCat]) + "," + y(d[yCat]) + ")";
  }
});



function tabulate(data, columns,id) {
    var table = d3.select(id).append('table').attr('class',"highlight")
    var thead = table.append('thead')
    var tbody = table.append('tbody');

    // append the header row
    thead.append('tr')
      .selectAll('th')
      .data(columns).enter()
      .append('th')
        .text(function (column) { return column; });

    // create a row for each object in the data
    var rows = tbody.selectAll('tr')
      .data(data)
      .enter()
      .append('tr');

    // create a cell in each row for each column
    var cells = rows.selectAll('td')
      .data(function (row) {
        return columns.map(function (column) {
          return {column: column, value: row[column]};
        });
      })
      .enter()
      .append('td')
        .text(function (d) { return d.value; });

    return table;
  }


d3.csv("/data/value_counts.csv", function (error,data) {
  // render the table(s)
  tabulate(data, ['Type', 'Count'],'#freq'); // 2 column table
});

d3.csv("/data/hot.csv", function (error,data) {
  // render the table(s)
  tabulate(data, ['Planet Name', 'Surface Temperature'],'#hot'); // 2 column table
});

d3.csv("/data/cold.csv", function (error,data) {
  // render the table(s)
  tabulate(data, ['Planet Name', 'Surface Temperature'],'#cold'); // 2 column table
});

d3.csv("/data/far.csv", function (error,data) {
  // render the table(s)
  tabulate(data, ['Planet Name', 'Distance from sun'],'#far'); // 2 column table
});

d3.csv("/data/near.csv", function (error,data) {
  // render the table(s)
  tabulate(data, ['Planet Name', 'Distance from sun'],'#near'); // 2 column table
});

d3.csv("/data/giant.csv", function (error,data) {
  // render the table(s)
  tabulate(data, ['Planet Name', 'Radius in Jpt'],'#giant'); // 2 column table
});

d3.csv("/data/little.csv", function (error,data) {
  // render the table(s)
  tabulate(data, ['Planet Name', 'Radius in Jpt'],'#little'); // 2 column table
});

d3.csv("/data/orphans.csv", function (error,data) {
  // render the table(s)
  tabulate(data, ['PlanetIdentifier', 'PlanetaryMassJpt',
       'DiscoveryYear', 'DistFromSunParsec'],'#orphans'); // 2 column table
});